﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag.Equals("floor")) {
            GameCommon.lifes--;       
            Destroy(gameObject);
        }
        if (other.gameObject.tag.Equals("barrier")) {
            Destroy(gameObject);
            Destroy(other.gameObject);
        }

    }
}