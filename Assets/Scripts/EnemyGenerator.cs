﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGenerator : MonoBehaviour {
	public GameObject enemyHot;
	public GameObject enemyCold;
	public float speed;
	public float deviation;
    float[] arr = { -11, -8, -5, -2, 1, 4, 7, 10 };
    private float countdown;

    void Start () {
		countdown = Random.Range(1, 7);
	}
	
	void Update () {
        if(GameCommon.isAlive) {
            countdown -= Time.deltaTime;
            if (countdown < 0)
                nextSpawn();
        }

	}

    void SpawnEnemy()
    {

		int rand;
		rand = Random.Range (1, 100);
		if (rand < 50) {
			GameObject anEnemy = Instantiate(enemyHot, transform.position, Quaternion.identity) as GameObject;
		} else {
			GameObject anEnemy = Instantiate(enemyCold, transform.position, Quaternion.identity) as GameObject;
		}
 
    }

    void nextSpawn() {
		countdown=  Random.Range(speed, speed+deviation);
        if (gameObject.tag.Equals("Boss") && GameCommon.isBossAlive) {
            SpawnEnemy();
        }else if(gameObject.tag.Equals("spawner") && !GameCommon.isBossAlive) {
            SpawnEnemy();
        }
           

    }

}
