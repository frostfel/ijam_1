﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPlatformMovement : MonoBehaviour {
    public Animator player;
    private bool isMoving;
    void Start()
    {
        isMoving = false;
    }
	void Update () {
        if(GameCommon.isAlive) {
            if (Input.GetKey(KeyCode.LeftArrow) && transform.position.x > -7.60f) {
                transform.Translate(-10 * Time.deltaTime, 0, 0);
                 isMoving = true;
                transform.localScale = new Vector3(-1, 1, 1) * 0.5f;
            }else if (Input.GetKey(KeyCode.RightArrow) && transform.position.x < 7.60f) {
                transform.Translate(10 * Time.deltaTime, 0, 0);
                isMoving = true;
                transform.localScale = new Vector3(1, 1, 1) * 0.5f;
            } else {
                isMoving = false;
            }


        }

    }
    void LateUpdate()
    {
        if (!isMoving) {
            if(GameCommon.powerColor.Equals("red")) {
                player.SetTrigger("idleFire");
            }else {
                player.SetTrigger("idleIce");
            }

        } else {
            if(GameCommon.powerColor.Equals("red")) {
                player.SetTrigger("walkingRightFire");
            }else {
                player.SetTrigger("walkingRightIce");
            }
            
        }
    }
}
