﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossSpawn : MonoBehaviour {

    public GameObject boss;

	
	void Update () {
        if (GameCommon.bossCount == 50) {
            GameCommon.isBossAlive = true;
            GameObject anEnemy = Instantiate(boss, transform.position, Quaternion.identity) as GameObject;
            GameCommon.bossCount = 0;
        }	
	}
}
