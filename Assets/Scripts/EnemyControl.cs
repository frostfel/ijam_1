﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyControl : MonoBehaviour {
    private float speed;
    private float countDown;
	// Use this for initialization
	void Start () {
        countDown = 3f;
        if (gameObject.tag.Equals("coldEnemy")) {
            speed = 0.5f*GameCommon.speed;
        }else {
            speed = 0.7f*GameCommon.speed;
        }

	}
	
	// Update is called once per frame
	void Update () {
        if(GameCommon.isAlive) {
            Vector2 position = transform.position;
            countDown -= Time.deltaTime;
            if(countDown < 0 ) {
                int prob = Random.Range(1, 10);
                if (prob > 7) {
                    position.x = position.x  + 50f * Time.deltaTime;
                } else if (prob > 5 && prob < 8) {
                    position.x = position.x - 50f *Time.deltaTime;
                }
                countDown = 3f;
            }

           
            if(GameCommon.isInBlueShrine && gameObject.tag.Equals("coldEnemy")) {
                position = new Vector2(position.x, position.y - speed * Time.deltaTime * 1.5f);

            } else if(GameCommon.isInRedShrine && gameObject.tag.Equals("hotEnemy")) {

                position = new Vector2(position.x, position.y - speed * Time.deltaTime * 1.5f);
            } else {

                position = new Vector2(position.x, position.y - speed * Time.deltaTime);
            }
            position = new Vector2(position.x, position.y - speed * Time.deltaTime);
            transform.position = position;

            Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));

        }
    }
    
}
