﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossControl : MonoBehaviour {
	float speed = 1;
	float ori;
    public int lifes;
	// Use this for initialization
	void Start () {
        lifes = 20;
	}
	
	// Update is called once per frame
	void Update () {
		
		if(GameCommon.isAlive && lifes > 0) {

			Vector2 position = transform.position;
			if (position.x <= -5) {
				speed = -1;
			} else if (position.x >= 5) {
				speed = 1;
			}


			position = new Vector2(position.x  - speed * Time.deltaTime, position.y);

			transform.position = position;

			Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
		}
        if(lifes < 1) {
            GameCommon.isBossAlive = false;
            Destroy(gameObject);
        }

	}
}
