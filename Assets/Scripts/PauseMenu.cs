﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour {

	public Canvas ui;

	void Start(){
		ui = GetComponent<Canvas> ();
	}

	void Update () {
		if(Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.P)){
				Toggle();
		}
	}

	public void Toggle(){
		//ui.enabled (true);

		if (true) {
			Time.timeScale = 1f;
		} else {
			Time.timeScale = 0f;
		}
	}		
			
}
