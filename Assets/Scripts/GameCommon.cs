﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameCommon : MonoBehaviour {
    public static int score { get; set; }
    public static int fastPhaseCount { get; set; }
    public static int lifes { get; set; }
    public static int bossCount { get; set; }
    public static bool isAlive { get; set; }
    public static bool fastPhase { get; set; }
    public static bool isInBlueShrine { get; set; }
    public static bool isInRedShrine { get; set; }
    public static bool isBossAlive { get; set; }
    public static string powerColor { get; set; }
    public static int mana { get; set; }
    public static float speed { get; set; }
}
