﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class PlayerController : MonoBehaviour {
    private string shootType;
    private float timeCount;
    private string changingTo;
    public bool isChanging;
    public GameObject hotProjectile;
    public GameObject coldProjectile;

    void Start () {
        isChanging = false;
        shootType = "hot";
        GameCommon.lifes = 5;
	}
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && GameCommon.isAlive) {
            shoot();
        }
        if (isChanging) timeCount += Time.deltaTime;
        if (timeCount > 1) {
            changeTypeOfShoot();
        }

    }
    void shoot()
    {
        if(GameCommon.mana > 0) {
            GameObject bullet;
            Vector3 bPosition = transform.position + new Vector3(0, 1, 0);
            switch (shootType) {
                case "hot":
                    bullet = Instantiate(hotProjectile, bPosition, Quaternion.identity) as GameObject;
                    break;
                case "cold":
                    bullet = Instantiate(coldProjectile, bPosition, Quaternion.identity) as GameObject;
                    break;
                default:
                    break;
            }
            GameCommon.mana--;
        }

    }
    void changeTypeOfShoot()
    {
        switch (changingTo) {
            case "coldResource":
                shootType = "cold";
                GameCommon.powerColor = "blue";
                break;
            case "hotResource":
                shootType = "hot";
                GameCommon.powerColor = "red";
                break;
            default:
                break;
        }
        GameCommon.mana = 10;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.name.Equals("hotResource")) {
            GameCommon.isInRedShrine = true;
        }else {
            GameCommon.isInBlueShrine = true;
        }
        isChanging = true;
        changingTo = other.gameObject.name;
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.name.Equals("hotResource")) {
            GameCommon.isInRedShrine = false;
        } else {
            GameCommon.isInBlueShrine = false;
        }
        timeCount = 0;
        isChanging = false;
    }

}
