﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreController : MonoBehaviour {
    public Text scoreText;
    public Text lifes;
    public Animator animator;
    public Animator playerAnim;
    public Sprite coldSprite;
    public Sprite hotSprite;
    public SpriteRenderer player;
    private float fastCount;
    private float messageTimeCount;
	void Start () {
        fastCount = 5f;
        GameCommon.score = 0;
        GameCommon.bossCount = 0;
        GameCommon.isBossAlive = false;
        GameCommon.isInBlueShrine = false;
        GameCommon.isInRedShrine = false;
        GameCommon.isAlive = true;
        GameCommon.powerColor = "red";
        GameCommon.mana = 10;
        GameCommon.fastPhaseCount = 0;
        GameCommon.fastPhase = false;
        GameCommon.speed = 1f;
        messageTimeCount = 15f;

    }
	
	void Update () {
        if(messageTimeCount < 0) {
            messageTimeCount = 30;
            animator.SetTrigger("ShowChat");
        }else {
            messageTimeCount -= Time.deltaTime;
        }

        if(GameCommon.powerColor.Equals("red")) {
            playerAnim.SetTrigger("idleFire");
            player.sprite = hotSprite;
        }else {
            player.sprite = coldSprite;
            playerAnim.SetTrigger("idleIce");
        }
        scoreText.text = "SCORE: "+GameCommon.score;
        lifes.text = "Lifes: " + GameCommon.lifes;
        if(GameCommon.fastPhase) {
            fastCount -= Time.deltaTime;
        }
        if(fastCount < 0) {
            GameCommon.fastPhase = false;
            GameCommon.speed = 1f;
            fastCount = 5f;
        }
        if(GameCommon.fastPhaseCount == 15) {
            GameCommon.fastPhase = true;
            GameCommon.fastPhaseCount = 0;
            GameCommon.speed = 1.5f;
        }
        if(GameCommon.lifes == 0) {
            GameCommon.isAlive = false;
        }

    }
}
