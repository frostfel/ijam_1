﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChatMessage : MonoBehaviour {

    public string message;
    public string type;

    public static ChatMessage CreateFromJSON(string jsonString)
    {
        return JsonUtility.FromJson<ChatMessage>(jsonString);
    }
}
