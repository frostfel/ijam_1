﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ManaBar : MonoBehaviour {
	public Image currentMana;

	private float maxMana = 10;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		float ratio = GameCommon.mana / maxMana;
		currentMana.rectTransform.localScale = new Vector3 (1, ratio, 1);
	}

}
