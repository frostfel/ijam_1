﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour {
    public GameObject boss;
	
	void Update () {
        transform.Translate(0, 15 * Time.deltaTime, 0);	
	}
    void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("Colision");
        if (gameObject.tag.Equals("hotBullet") && other.gameObject.tag.Equals("hotEnemy")) {
            scorePoint(other);
        }
        if (gameObject.tag.Equals("hotBullet") && other.gameObject.tag.Equals("coldEnemy")) {
            penalize();
        }
        if (gameObject.tag.Equals("coldBullet") && other.gameObject.tag.Equals("hotEnemy")) {
            penalize();
        }
        if (gameObject.tag.Equals("coldBullet") && other.gameObject.tag.Equals("coldEnemy")) {
            scorePoint(other);
        }
        if(GameCommon.isBossAlive && other.gameObject.tag.Equals("Boss")) {
            hitBoss(other.gameObject);
        }
    }
    void hitBoss(GameObject boss)
    {
        BossControl bossControl = boss.gameObject.GetComponent<BossControl>();
        bossControl.lifes--;
        Destroy(gameObject);
    }
    void penalize()
    {
        GameCommon.score -= 3; 
        Destroy(gameObject);
    }
    void scorePoint(Collider2D other)
    {
        GameCommon.bossCount++;
        Destroy(other.gameObject);
        if(!GameCommon.fastPhase) GameCommon.fastPhaseCount++;    
        GameCommon.score ++;
        Destroy(gameObject);
    }
}
