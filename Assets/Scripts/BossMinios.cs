﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossMinios : MonoBehaviour {
	float speed = 0.1f;
	float yaw = 0.1f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(GameCommon.isAlive) {
			Vector2 position = transform.position;
			if (position.x <= -0.5) {
				yaw = -1;
			} else if (position.x >= 0.5) {
				yaw = 1;
			}
			position = new Vector2(position.x - yaw * Time.deltaTime, position.y - speed * Time.deltaTime);
			transform.position = position;

			Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));

		}
	}
}
